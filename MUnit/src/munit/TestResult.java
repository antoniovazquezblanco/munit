/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package munit;

public class TestResult {
    public String file_name;
    public int file_line;
    public String test_name;
    public boolean test_result;
    
    public TestResult(Report r) {
        if (r.report_type != Report.Type.REPORT_TYPE_TESTRESULT) {
            throw new IllegalArgumentException("Report is not a test result");
        }
        file_name = r.file_name;
        file_line = r.file_line;
        test_name = r.test_name;
        test_result = r.test_result;
    }
    
    public void printResult() {
        System.out.println("Test " + test_name + " in " + file_name + ":" + file_line + " " + ((test_result) ? "succeeded" : "failed"));
    }
}
