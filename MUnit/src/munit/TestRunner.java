/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package munit;

import com.microchip.mplab.mdbcore.assemblies.Assembly;
import com.microchip.mplab.mdbcore.assemblies.AssemblyFactory;
import com.microchip.mplab.mdbcore.codecoveragecore.Coverage;
import com.microchip.mplab.mdbcore.codecoveragecore.info.CodeCoverageCoreReport;
import com.microchip.mplab.mdbcore.codecoveragecore.info.CodeCoverageInfo;
import com.microchip.mplab.mdbcore.codecoveragecore.info.CoverageInfo;
import com.microchip.mplab.mdbcore.debugger.DebugException;
import com.microchip.mplab.mdbcore.debugger.Debugger;
import com.microchip.mplab.mdbcore.debugger.MDBDebugTool;
import com.microchip.mplab.mdbcore.debugger.ToolEvent;
import com.microchip.mplab.mdbcore.loader.LoadException;
import com.microchip.mplab.mdbcore.loader.Loader;
import com.microchip.mplab.mdbcore.platformtool.PlatformToolMeta;
import com.microchip.mplab.mdbcore.simulator.CodeCoverage;
import com.microchip.mplab.mdbcore.simulator.Simulator;
import com.microchip.mplab.mdbcore.symbolview.interfaces.SymbolInfo;
import com.microchip.mplab.mdbcore.symbolview.interfaces.SymbolViewProvider;
import com.microchip.mplab.mdbcore.translator.exceptions.TranslatorException;
import com.microchip.mplab.mdbcore.translator.interfaces.CallStackFrame;
import com.microchip.mplab.mdbcore.translator.interfaces.CallStackSupport;
import com.microchip.mplab.mdbcore.translator.interfaces.ITranslator;
import com.microchip.mplab.mdbcore.translator.interfaces.ITranslator.AddrToLn;
import com.microchip.mplab.util.observers.Observer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import munit.mdbhelpers.Event;
import munit.mdbhelpers.SymbolInfoParser;
import munit.mdbhelpers.Tool;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;

/**
 * Test runner class. This class handles the execution of a particular test
 * project, collecting each assertion result into memory. It basically
 * instaniates an Microchip Simulator tool instance and runs the provided
 * program listening for Simulator events. Each time an assertion is executed
 * the C code in the uC will hit a breakpoint launching a simulator event. We
 * will look for the report information and resume execution until a tests
 * finished report is found.
 */
public class TestRunner implements Observer {

    private Assembly mAssembly;
    private Coverage mCoverage;
    private CodeCoverage mCodeCoverage;
    private Debugger mMdb;
    private ITranslator mTranslator;
    private SymbolViewProvider mSymbolView;
    private Event mDoneEvent;
    private TestState mState;
    private List<TestResult> mTestResults;

    public TestRunner(String device, String firmware) {
        // Internal testing state...
        mState = TestState.UNKNOWN;
        mTestResults = new ArrayList<TestResult>();

        // Initialize event objs...
        mDoneEvent = new Event();

        // Load the corresponding device assembly...
        AssemblyFactory assemblyFactory = Lookup.getDefault().lookup(AssemblyFactory.class);
        mAssembly = assemblyFactory.Create(device);

        // Change tool to Simulator...
        PlatformToolMeta toolMeta = Tool.getToolMetaFromName("Simulator");
        String toolId = Tool.getConnectedToolIdFromName("Simulator");
        assemblyFactory.ChangeTool(mAssembly, toolMeta.getConfigurationObjectID(), toolMeta.getClassName(), toolMeta.getFlavor(), toolId);

        // Obtain a code translator...
        mTranslator = mAssembly.getLookup().lookup(ITranslator.class);

        // Obtain a symbol viewer...
        mSymbolView = mAssembly.getLookup().lookup(SymbolViewProvider.class);

        // Lookup a debugger...
        mMdb = mAssembly.getLookup().lookup(Debugger.class);
        try {
            mMdb.Connect(Debugger.CONNECTION_TYPE.DEBUGGER);
        } catch (DebugException ex) {
            Exceptions.printStackTrace(ex);
        }

        // Listen to debugger events...
        mMdb.Attach(this, null);

        // Load the test firmware...
        Loader loader = mAssembly.getLookup().lookup(Loader.class);
        try {
            loader.Load(firmware);
        } catch (LoadException ex) {
            Exceptions.printStackTrace(ex);
        }

        try {
            // Program target device...
            mMdb.Program(Debugger.PROGRAM_OPERATION.AUTO_SELECT);
        } catch (DebugException ex) {
            Exceptions.printStackTrace(ex);
        }

        MDBDebugTool tool = mAssembly.getLookup().lookup(MDBDebugTool.class);
        Simulator sim = (Simulator) tool;
        sim.startCodeCoverage();

        mCodeCoverage = (CodeCoverage) tool;
        mCoverage = mAssembly.getLookup().lookup(Coverage.class);
    }

    public void run() throws InterruptedException {
        mMdb.Run();
        mDoneEvent.await();
        mMdb.Disconnect();
    }

    @Override
    public void Update(Object o) {
        if (o instanceof ToolEvent) {
            switch (((ToolEvent) o).GetEvent()) {
                case HALT:
                    stateUpdate(Signal.MDB_HALTED);
                    break;
                /*TODO: default:
                    System.out.println("[D] mMdb: default: " + ((ToolEvent) o).GetEvent());
                    break;*/
            }
        }/* TODO else {
            System.out.println("[D] mMdb uknown object instance: " + o.toString());
        } */
    }

    private boolean wasHaltDueToReport() {
        CallStackSupport stack = mTranslator.getCallStackSupportProvider();
        try {
            List<CallStackFrame> frames = stack.resolveCallStackFrames(mMdb.GetPC(), false);
            for (CallStackFrame f : frames) {
                if (f.func.equals("_munit_trigger_report")) {
                    return true;
                }
            }
        } catch (TranslatorException ex) {
            Exceptions.printStackTrace(ex);
        }
        return false;
    }

    private Report getLatestReport() {
        try {
            SymbolInfo si = mSymbolView.resolve("_munit_global_report", mMdb.GetPC(), false);
            return SymbolInfoParser.value(mSymbolView, si, Report.class);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    private String getCurrentSourceCodeLine() {
        long currentPc = mMdb.GetPC() & 0x00000000ffffffffL;
        try {
            AddrToLn info = mTranslator.addressToSourceLine(currentPc);
            return info.file + ":" + String.valueOf(info.lLine);
        } catch (TranslatorException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    private enum TestState {
        UNKNOWN,
        CHECK_HALT_REASON,
        PROCESS_REPORT,
        AWAIT_END,
        EXIT,
        EXIT_BAD
    }

    private enum Signal {
        UNKNOWN,
        MDB_HALTED,
    }

    private void stateUpdate(Signal s) {
        switch (mState) {
            case UNKNOWN:
                if (s == Signal.MDB_HALTED) {
                    mState = TestState.CHECK_HALT_REASON;
                    stateUpdate(Signal.UNKNOWN);
                }
                break;
            case CHECK_HALT_REASON:
                if (wasHaltDueToReport()) {
                    mState = TestState.PROCESS_REPORT;
                    stateUpdate(Signal.UNKNOWN);
                } else {
                    System.out.println("[!] Debugger halted due to unknown reasons at:");
                    System.out.println(getCurrentSourceCodeLine());
                    mState = TestState.EXIT_BAD;
                    stateUpdate(Signal.UNKNOWN);
                }
                break;
            case PROCESS_REPORT:
                Report r = getLatestReport();
                switch (r.report_type) {
                    case REPORT_TYPE_TESTSSTARTED:
                        System.out.println("[+] Starting unit tests...");
                        mMdb.Run();
                        break;
                    case REPORT_TYPE_TESTSFINISHED:
                        System.out.println("[+] Tests finished!");
                        mState = TestState.AWAIT_END;
                        mMdb.Run();
                        break;
                    case REPORT_TYPE_TESTRESULT:
                        mTestResults.add(new TestResult(r));
                        mMdb.Run();
                        break;
                    default:
                        System.out.println("[!] Unknown report type received");
                        mState = TestState.EXIT_BAD;
                        stateUpdate(Signal.UNKNOWN);
                        break;
                }
                break;
            case AWAIT_END:
                int good = 0;
                for (TestResult t : mTestResults) {
                    t.printResult();
                    if (t.test_result) {
                        good++;
                    }
                }
                int total = mTestResults.size();
                System.out.println(String.format("Success: %.2f%% (%d / %d)", good * 100.0 / total, good, total));
                System.out.println("Code coverage report:");
                // Dirty coverage report triggering
                CodeCoverageCoreReport rep = new CodeCoverageCoreReport(mCodeCoverage, mAssembly, InputOutput.NULL);
                rep.GenerateSimulatorReport();
                Map<String, CodeCoverageInfo> ccinfo = mCoverage.getCCInfo();
                long executed_addrs = 0;
                long total_addrs = 0;
                for (CodeCoverageInfo cci : ccinfo.values()) {
                    for (CoverageInfo fcci : cci.getFunctionCoverageInfo().values()) {
                        executed_addrs += fcci.getAddressExecuted();
                        total_addrs += fcci.getAddressNotExecuted();
                    }
                    System.out.println(cci.getFileName() + ": " + cci.getPercentageFromAddressCovered());
                }
                total_addrs += executed_addrs;
                System.out.println(String.format("Total coverage: %.2f%%", executed_addrs * 100.0 / total_addrs));
                mState = (good == total) ? TestState.EXIT : TestState.EXIT_BAD;
                stateUpdate(Signal.UNKNOWN);
                break;
            case EXIT:
                System.out.println("[+] Exiting...");
                mDoneEvent.set();
                break;
            case EXIT_BAD:
                System.out.println("[!] There are failed tests!");
                System.exit(1);
                break;
        }
    }
}
