/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package munit.mdbhelpers;

import com.microchip.mplab.mdbcore.MessageMediator.ActionList;
import com.microchip.mplab.mdbcore.MessageMediator.Message;
import com.microchip.mplab.mdbcore.MessageMediator.MessageMediatorListener;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/**
 * This class implements the interface to handle incoming messages from the
 * MDBCore software. This messages mostly should be redirected to the user but
 * some of them may require interaction or user input.
 *
 * Documentation about this can be found by downloading MPLAB X SDK and looking
 * for the classes ActionList and Message in the package
 * com.microchip.mplab.mdbcore.MessageMediator. That information is located in
 * the file mplab-x-sdk/MPLAB_X/doc/mplab-core/index.html
 */
@ServiceProvider(service = MessageMediatorListener.class)
public class MDBCoreMessageMediatorListener implements MessageMediatorListener {

    private static final Logger LOGGER = Logger.getLogger(MDBCoreMessageMediatorListener.class.getName());

    @Override
    public int handleMessage(Message msg, int actionId) {
        switch (actionId) {
            case ActionList.OutputWindowOnly:
            case ActionList.OutputWindowOnlyDisplayMessage:
            case ActionList.OutputWindowOnlyDisplayColor:
            case ActionList.FocusOnViewOnly:
            case ActionList.FocusOnViewAndOutPutWindowMessage:
            case ActionList.FocusOnViewAndOutPutWindowColor: {
                LOGGER.log(Level.FINE, msg.getMessageString());
                return -1;
            }
            case ActionList.DialogPopupOnly:
            case ActionList.DialogPopupAndOutputWindowMessage:
            case ActionList.DialogPopupAndOutputWindowError:
            case ActionList.DialogPopupAndOutputWindowColor:
            case ActionList.DialogPopupAndOutputWindowErrorLink:
            case ActionList.FocusOnViewDialogPopupAndOutputWindowMessage:
            case ActionList.FocusOnViewDialogPopupAndOutputWindowError:
            case ActionList.FocusOnViewDialogPopupAndOutputWindowColor:
            case ActionList.FocusOnViewDialogPopupAndOutputWindowErrorLink: {
                Scanner Input = new Scanner(System.in);
                System.out.println(msg.getMessageString());
                while (true) {
                    String response = Input.nextLine();
                    if (response.equalsIgnoreCase("Yes")) {
                        return 0;
                    } else if (response.equalsIgnoreCase("No")) {
                        return 1;
                    } else {
                        System.out.println("Invalid response");
                    }
                }
            }
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
