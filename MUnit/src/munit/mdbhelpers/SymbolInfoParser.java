/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package munit.mdbhelpers;

import com.microchip.mplab.mdbcore.symbolview.exceptions.SymbolViewException;
import com.microchip.mplab.mdbcore.symbolview.interfaces.SymbolInfo;
import com.microchip.mplab.mdbcore.symbolview.interfaces.SymbolViewProvider;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Symbol information parser class.
 * This class deserializes a Simulator memory portion into a Java object. The
 * aim of this class is to be able to parse simulated device memory into objects
 * that can be correctly handled in Java.
 */
public class SymbolInfoParser {

    public static <T> T value(SymbolViewProvider sv, SymbolInfo s, Class<T> objectType)
            throws IllegalArgumentException, InstantiationException, IllegalAccessException, NoSuchFieldException, SymbolViewException, NoSuchMethodException, InvocationTargetException {
        if (objectType.equals(boolean.class) || objectType.equals(Boolean.class)) {
            return (T) valueToBool(sv, s);
        } else if (objectType.equals(int.class) || objectType.equals(Integer.class)) {
            return (T) valueToInt(sv, s);
        } else if (objectType.equals(char.class) || objectType.equals(Character.class)) {
            return (T) valueToChar(sv, s);
        } else if (objectType.equals(String.class)) {
            return (T) valueToString(sv, s);
        } else if (objectType.isEnum()) {
            return valueToEnum(sv, s, objectType);
        } else if (s.isStruct()) {
            return valueToStruct(sv, s, objectType);
        }
        throw new IllegalArgumentException("Unsupported type: " + objectType.getSimpleName());
    }

    /**
     * Resolve the specified symbol info via the provided symbol view provider and convert it to a boolean type.
     * @param sv MDB symbol view provider.
     * @param s MDB symbol info to be resolved.
     * @return Symbol value.
     * @throws SymbolViewException If the symbol cannot be found or resolved.
     */
    public static Boolean valueToBool(SymbolViewProvider sv, SymbolInfo s) throws SymbolViewException {
        sv.readValueInformation(s);
        return (boolean) (s.getIntegalValue() != 0);
    }

    /**
     * Resolve the specified symbol info via the provided symbol view provider and convert it to an Integer type.
     * @param sv MDB symbol view provider.
     * @param s MDB symbol info to be resolved.
     * @return Symbol value.
     * @throws SymbolViewException If the symbol cannot be found or resolved.
     */
    public static Integer valueToInt(SymbolViewProvider sv, SymbolInfo s) throws SymbolViewException {
        sv.readValueInformation(s);
        return (int) s.getIntegalValue();
    }

    /**
     * Resolve the specified symbol info via the provided symbol view provider and convert it to an Character type.
     * @param sv MDB symbol view provider.
     * @param s MDB symbol info to be resolved.
     * @return Symbol value.
     * @throws SymbolViewException If the symbol cannot be found or resolved.
     */
    public static Character valueToChar(SymbolViewProvider sv, SymbolInfo s) throws SymbolViewException {
        if (!s.isChar()) {
            throw new IllegalArgumentException("Symbol is not a character");
        }
        sv.readValueInformation(s);
        return (char) s.getIntegalValue();
    }

    /**
     * Resolve the specified symbol info via the provided symbol view provider and convert it to an String type.
     * @param sv MDB symbol view provider.
     * @param s MDB symbol info to be resolved.
     * @return Symbol value.
     * @throws SymbolViewException If the symbol cannot be found or resolved.
     */
    public static String valueToString(SymbolViewProvider sv, SymbolInfo s) throws SymbolViewException {
        if (!s.isArray()) {
            throw new IllegalArgumentException("Symbol is not an array for a string");
        }
        StringBuilder str = new StringBuilder();
        for (SymbolInfo character : s.getMembers()) {
            char c = valueToChar(sv, character);
            str.append(c);
            if (c == '\0') {
                break;
            }
        }
        return str.toString();
    }

    public static <T> T valueToStruct(SymbolViewProvider sv, SymbolInfo s, Class<T> objectType)
            throws InstantiationException, IllegalAccessException, NoSuchFieldException, IllegalArgumentException, SymbolViewException, NoSuchMethodException, InvocationTargetException {
        if (!s.isStruct()) {
            throw new IllegalArgumentException("Symbol is not a structure");
        }
        T object = objectType.newInstance();
        for (SymbolInfo structField : s.getMembers()) {
            Field objectField = object.getClass().getDeclaredField(structField.getUnQualifiedViewName());
            Object val = value(sv, structField, objectField.getType());
            objectField.set(object, val);
        }
        return object;
    }

    public static <T> T valueToEnum(SymbolViewProvider sv, SymbolInfo s, Class<T> objectType) throws SymbolViewException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (!s.isEnum()) {
            throw new IllegalArgumentException("Symbol is not an enum");
        }
        if (!objectType.isEnum()) {
            throw new IllegalArgumentException(objectType + " is not an enum");
        }
        sv.readValueInformation(s);
        Method method = objectType.getDeclaredMethod("values");
        Object obj[] = (Object[]) method.invoke(null);
        return (T) obj[(int) s.getIntegalValue()];
    }
}
