/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package munit.mdbhelpers;

/**
 * Event class.
 * This class is a simple implementation of a synchronized flag that can be set,
 * reseted and awaited. Helps our program to synchronize with MDB elements that
 * have their own threading.
 */
public class Event {

    private final Object mLock;
    private boolean mEventSet;

    /**
     * Create a new event object.
     */
    public Event() {
        mLock = new Object();
        mEventSet = false;
    }

    /**
     * Clear the event.
     */
    public void reset() {
        synchronized (mLock) {
            mEventSet = false;
            mLock.notifyAll();
        }
    }

    /**
     * Set the event.
     */
    public void set() {
        synchronized (mLock) {
            mEventSet = true;
            mLock.notifyAll();
        }
    }

    /**
     * Lock the current thread until the event is set.
     * @throws InterruptedException 
     */
    public void await() throws InterruptedException {
        synchronized (mLock) {
            while (!mEventSet) {
                mLock.wait();
            }
        }
    }
}
