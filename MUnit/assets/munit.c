/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "munit.h"

#include <string.h>

// Maximum string size
#define MAX_CHARS 100

/**
 * Enum representing possible MUnit report types.
 */
typedef enum {
    REPORT_TYPE_UNDEF = 0,
    REPORT_TYPE_TESTSSTARTED = 1,
    REPORT_TYPE_TESTSFINISHED = 2,
    REPORT_TYPE_TESTRESULT = 3,
} _munit_report_type_t;

/**
 * This struct represents a message between the uC and MUnit.jar.
 */
struct _munit_report {
    _munit_report_type_t report_type;
    char file_name[MAX_CHARS];
    int file_line;
    char test_name[MAX_CHARS];
    bool test_result;
};

static struct _munit_report _munit_global_report;

/**
 * Halts execution in order to notify MUnit.jar that a report is available.
 */
static void _munit_trigger_report() {
#if !defined(__DEBUG)
    __builtin_software_breakpoint();
#endif
    __builtin_nop();
}

void munit_tests_start() {
    _munit_global_report.report_type = REPORT_TYPE_TESTSSTARTED;
    _munit_trigger_report();
}

void munit_tests_finish() {
    _munit_global_report.report_type = REPORT_TYPE_TESTSFINISHED;
    _munit_trigger_report();
}

void _munit_assert(char *file, int line, char *name, bool assertion) {
    _munit_global_report.report_type = REPORT_TYPE_TESTRESULT;
    strncpy((char *) &_munit_global_report.file_name[0], file, MAX_CHARS);
    _munit_global_report.file_line = line;
    strncpy((char *) &_munit_global_report.test_name[0], name, MAX_CHARS);
    _munit_global_report.test_result = assertion;
    _munit_trigger_report();
}
