/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _H_MUNIT
#define _H_MUNIT

#include <stdbool.h>

/**
 * Signals the start of a testsuite.
 */
void munit_tests_start();

/**
 * Call whenever you want to end a test session.
 */
void munit_tests_finish();

/**
 * Assertion macro for test performing. It takes a test name in the form of a
 * string and the tested condition.
 * An example call could be:
 *   munit_assert("test1", sizeof(variable) == 2);
 */
#define munit_assert(name, boolean) _munit_assert(__FILE__, __LINE__, name, boolean)

/**
 * Assertion function definition. For internal use only. Please use the 
 * `munit_assert` macro.
 */
void _munit_assert(char *file, int line, char *name, bool assertion);

#endif
