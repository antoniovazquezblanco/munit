# MUnit

[![pipeline status](https://gitlab.com/stemy-energy/hardware/libraries/munit/badges/master/pipeline.svg)](https://gitlab.com/stemy-energy/hardware/libraries/munit/-/commits/master)

MUnit is an unit testing tool that integrates with Microchip MPLAB X IDE.


## Compiling

In order to compile MUnit you will need `JDK 8` and either `ant` or `Netbeans`. Development is done using `Apache Netbeans 12.2`.
Compilation will generate a dist folder containing all you need to use this program.


## Using

### Setup your project.

There is an already setup project in the examples folder that you can use. If you want to setup your own project just follow theese steps:

1. Copy the distribution files into a folder named `munit` inside an existing MPLAB X project.
2. Configure the MPLAB X project to compile `munit.c` and `munit.h`.
3. Add `munit.mk` to the important files section of your project.
4. Append the line `include munit/munit.mk` to the end of your project `Makefile` in the important files section.
5. Call the `munit_tests_start();` function before performing your tests.
6. Use the `munit_assert(name, boolean)` macro to perform your tests.
7. End your program by calling `munit_tests_finish();`.


### Running and debugging your tests.

Every time you perform a `Build` or `Clean and build` operation the test suite will be run in Microchip Simulator. The output window will show the result report.

If a test fails, you can allways choose to setup a breakpoint and run the test suite in debug mode. This will allow you to debug a particular test without interruption.
